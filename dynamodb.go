package eventime

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
)

type DynamoDB struct {
	Client dynamodbiface.DynamoDBAPI

	Table string
	Data  ByCity
}

func (db *DynamoDB) Load(keys ...string) error {
	values := make([]map[string]*dynamodb.AttributeValue, len(keys))

	for i, key := range keys {
		values[i] = map[string]*dynamodb.AttributeValue{
			"CityName": &dynamodb.AttributeValue{S: aws.String(key)},
		}
	}

	input := &dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			db.Table: &dynamodb.KeysAndAttributes{
				ExpressionAttributeNames: map[string]*string{
					"#TimeZoneJSON": aws.String("TimeZoneJSON"),
				},
				Keys:                 values,
				ProjectionExpression: aws.String("#TimeZoneJSON"),
			},
		},
	}

	response, err := db.Client.BatchGetItem(input)

	if err != nil {
		return err
	}

	result, ok := response.Responses[db.Table]
	if !ok {
		return fmt.Errorf("missing data")
	}

	return db.loadResult(result)
}

func (db *DynamoDB) Save() error {
	return nil
}

func (db *DynamoDB) loadResult(items []map[string]*dynamodb.AttributeValue) error {
	for _, item := range items {
		var data []byte

		jsonData := item["TimeZoneJSON"]

		if jsonValue := jsonData.B; jsonValue != nil {
			data = jsonValue
		} else {
			return fmt.Errorf("missing data")
		}

		var tmp TimeZone
		if err := json.Unmarshal(data, &tmp); err != nil {
			return err
		}

		db.Data = append(db.Data, tmp)
	}

	return nil
}

func (z *DynamoDB) Zones() ByGMT {
	return ByGMT(z.Data)
}

func (z *DynamoDB) Cities() ByCity {
	return z.Data
}

func (db *DynamoDB) Copy(other ZoneInfo) {
	db.Data = other.Cities()
}

func (s *DynamoDB) Configure(client interface{}) error {
	defer func() error {
		if r := recover(); r != nil {
			return fmt.Errorf("unable to configure with given client")
		}
		return nil
	}()

	s.Client = (client).(dynamodbiface.DynamoDBAPI)
	return nil
}
