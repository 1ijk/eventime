package eventime

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var (
	newyork, _ = NewTimeZone("New_York", "America/New_York")
	tokyo, _   = NewTimeZone("Tokyo", "Asia/Tokyo")
	chicago, _ = NewTimeZone("Chicago", "America/Chicago")
	madrid, _  = NewTimeZone("Madrid", "Europe/Madrid")
	london, _  = NewTimeZone("London", "Europe/London")

	cities = ByCity{chicago, london, madrid, newyork, tokyo}
)

func winterGMTForLoc(location *time.Location) time.Time {
	return time.Date(2020, time.January, 1, 12, 0, 0, 0, location)
}

func winterInCity(cityName string) TimeZone {
	city, _ := cities.FindCity(cityName)
	city.time = winterGMTForLoc(city.zone)
	return city
}

func summerGMTForLoc(location *time.Location) time.Time {
	return time.Date(2020, time.July, 1, 12, 0, 0, 0, location)
}

func summerInCity(cityName string) TimeZone {
	city, _ := cities.FindCity(cityName)
	city.time = summerGMTForLoc(city.zone)
	return city
}

func clockTimes() []string {
	times := make([]string, 24, 24)

	times[0] = "12:00AM"
	times[12] = "12:00PM"

	for i := 1; i < 12; i++ {
		times[i] = fmt.Sprintf("%d:00AM", i)
	}

	for i := 13; i < 24; i++ {
		times[i] = fmt.Sprintf("%d:00PM", i%12)
	}

	return times
}

func rotate(times []string, amount int) []string {
	if amount == 0 {
		return times
	}

	if amount < 0 {
		return rotate(times, len(times)+amount)
	}

	result := make([]string, len(times), cap(times))

	result = append(times[amount:], times[:amount-1]...)

	return result
}

func Test_rotate(t *testing.T) {
	assert.Equal(t, "11:00PM", rotate(clockTimes(), -1)[0])
	assert.Equal(t, "12:00AM", rotate(clockTimes(), 0)[0])
	assert.Equal(t, "1:00AM", rotate(clockTimes(), 1)[0])
	assert.Equal(t, "12:00PM", rotate(clockTimes(), 12)[0])
	assert.Equal(t, "1:00PM", rotate(clockTimes(), 13)[0])
	assert.Equal(t, "12:00AM", rotate(clockTimes(), 24)[0])
}
