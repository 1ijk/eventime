package eventime

import (
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v7"
)

type Redis struct {
	Client *redis.Client
	Data   ByCity
}

func (r *Redis) Load(cities ...string) error {
	data, err := r.Client.MGet(cities...).Result()
	if err != nil {
		return err
	}

	for i := 0; i < len(data); i++ {
		var tmp TimeZone
		if err := json.Unmarshal((data[i]).([]byte), &tmp); err != nil {
			return err
		}
		r.Data = append(r.Data, tmp)
	}

	return nil
}

func (r *Redis) Save() error {
	dto := make(map[string][]byte, r.Data.Len())

	for i := 0; i < r.Data.Len(); i++ {
		data, err := json.Marshal(r.Data[i])
		if err != nil {
			return err
		}
		dto[r.Data[i].city] = data
	}

	_, err := r.Client.MSet(dto).Result()

	return err
}

func (z *Redis) Zones() ByGMT {
	return ByGMT(z.Data)
}

func (z *Redis) Cities() ByCity {
	return z.Data
}

func (r *Redis) Copy(other ZoneInfo) {
	r.Data = other.Cities()
}

func (s *Redis) Configure(client interface{}) error {
	defer func() error {
		if r := recover(); r != nil {
			return fmt.Errorf("unable to configure with given client")
		}
		return nil
	}()

	s.Client = (client).(*redis.Client)
	if _, err := s.Client.Ping().Result(); err != nil {
		return err
	}
	return nil
}
