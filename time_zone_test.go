package eventime

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var (
	cityTests = map[string]TimeZone{
		"Chicago":  chicago,
		"New York": newyork,
		"Madrid":   madrid,
		"Tokyo":    tokyo,
	}
	daylightSavingsTests = map[string]TimeZone{
		"GMT-6": winterInCity("Chicago"),
		"GMT-5": summerInCity("Chicago"),
		"GMT+1": winterInCity("Madrid"),
		"GMT+2": summerInCity("Madrid"),
	}
	noDaylightSavings = map[string]string{
		"Tokyo": "GMT+9",
	}
	winterClockTests = map[string]TimeZone{
		"6:00AM": winterInCity("Chicago"),
		"1:00PM": winterInCity("Madrid"),
		"9:00PM": winterInCity("Tokyo"),
	}
	summerClockTests = map[string]TimeZone{
		"7:00AM": summerInCity("Chicago"),
		"2:00PM": summerInCity("Madrid"),
		"9:00PM": summerInCity("Tokyo"),
	}
	winterOffsetTests = map[time.Duration]TimeZone{
		-6 * time.Hour: winterInCity("Chicago"),
		time.Hour:      winterInCity("Madrid"),
		9 * time.Hour:  winterInCity("Tokyo"),
	}
	summerOffsetTests = map[time.Duration]TimeZone{
		-5 * time.Hour: summerInCity("Chicago"),
		2 * time.Hour:  summerInCity("Madrid"),
		9 * time.Hour:  summerInCity("Tokyo"),
	}
)

func TestTimeZone_City(t *testing.T) {
	for name, city := range cityTests {
		assert.Equal(t, name, city.City())
	}
}

func TestTimeZone_GMT(t *testing.T) {
	for gmt, city := range daylightSavingsTests {
		assert.Equal(t, gmt, city.GMT(), "wrong GMT for %v", city)
	}

	for city, gmt := range noDaylightSavings {
		summerTime := summerInCity(city)
		assert.Equal(t, gmt, summerTime.GMT(), "wrong summertime GMT for %v", city)
		winterTime := winterInCity(city)
		assert.Equal(t, gmt, winterTime.GMT(), "wrong wintertime GMT for %v", city)

	}
}

func TestTimeZone_Clock(t *testing.T) {
	for _, city := range winterClockTests {
		assert.Equal(t, "12:00PM", city.Clock(), "incorrect kitchen time for %v, city")
	}

	for _, city := range summerClockTests {
		assert.Equal(t, "12:00PM", city.Clock(), "incorrect kitchen time for %v, city")
	}

	for clock, city := range winterClockTests {
		city = city.Set(winterGMTForLoc(time.UTC))
		assert.Equal(t, clock, city.Clock(), "incorrect kitchen time for %v", city)
	}

	for clock, city := range summerClockTests {
		city = city.Set(summerGMTForLoc(time.UTC))
		assert.Equal(t, clock, city.Clock(), "incorrect kitchen time for %v", city)
	}
}

func TestTimeZone_Offset(t *testing.T) {
	for offset, city := range winterOffsetTests {
		assert.Equal(t, offset, city.Offset(), "incorrect winter offset for %v", city)
	}

	for offset, city := range summerOffsetTests {
		assert.Equal(t, offset, city.Offset(), "incorrect summer offset for %v", city)
	}
}
