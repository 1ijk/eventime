module gitlab.com/1ijk/eventime

go 1.14

require (
	cloud.google.com/go/storage v1.6.0
	github.com/aws/aws-sdk-go v1.30.17
	github.com/go-redis/redis/v7 v7.2.0
	github.com/gorilla/mux v1.7.4
	github.com/magiconair/properties v1.8.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.3
	github.com/stretchr/testify v1.5.1
)
