package eventime

import (
	"encoding/json"
	"io"
)

type JSON struct {
	File io.ReadWriteCloser
	Data ByCity
}

func (data *JSON) Load(cities ...string) error {
	decoder := json.NewDecoder(data.File)

	var tmp ByCity
	if err := decoder.Decode(&tmp); err != nil {
		return err
	}

	data.Data = tmp.Filter(cities...)

	return nil
}

func (j *JSON) Save() error {
	encoder := json.NewEncoder(j.File)
	return encoder.Encode(j.Data)
}

func (z *JSON) Zones() ByGMT {
	return ByGMT(z.Data)
}

func (z *JSON) Cities() ByCity {
	return z.Data
}

func (j *JSON) Copy(other ZoneInfo) {
	j.Data = other.Cities()
}

func (j *JSON) Configure(interface{}) error {
	return nil
}
