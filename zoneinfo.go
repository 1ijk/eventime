package eventime

import (
	"fmt"
	"os"
	"sort"
)

type ZoneInfo interface {
	Configure(connection interface{}) error
	Load(keys ...string) error
	Save() error
	Copy(other ZoneInfo)
	Zones() ByGMT
	Cities() ByCity
}

const (
	KindUnix = iota
	KindText
	KindJOSN
	KindRedis
	KindDynamoDB
	KindStorage
)

var (
	RequireConfiguration = fmt.Errorf("this ZoneInfo type requires client configuration")
	ErrUnknownZoneInfo   = fmt.Errorf("unknown kind for zone info")
)

func New(location string, kind int) (ZoneInfo, error) {
	switch kind {
	case KindUnix:
		return &Unix{Directory: location}, nil
	case KindText:
		file, err := os.OpenFile(location, os.O_CREATE|os.O_RDWR, os.ModePerm)
		return &Text{File: file}, err
	case KindJOSN:
		file, err := os.OpenFile(location, os.O_CREATE|os.O_RDWR, os.ModePerm)
		return &JSON{File: file}, err
	case KindRedis:
		return &Redis{}, RequireConfiguration
	case KindDynamoDB:
		return &DynamoDB{Table: location}, RequireConfiguration
	case KindStorage:
		return &Storage{ObjectName: location}, RequireConfiguration
	default:
		return nil, ErrUnknownZoneInfo
	}
}

func include(keys []string, key string) bool {
	if len(keys) == 0 {
		return true
	}

	sort.Strings(keys)
	// return true when the key exists
	i := sort.SearchStrings(keys, key)
	return len(keys) > i && keys[i] == key
}
