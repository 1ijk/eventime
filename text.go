package eventime

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"io"
)

type Text struct {
	File io.ReadWriteCloser
	Data ByCity
}

func (t *Text) Load(keys ...string) error {
	scanner := bufio.NewScanner(t.File)

	for scanner.Scan() {
		record := scanner.Bytes()

		var tmp data
		decoder := gob.NewDecoder(bytes.NewBuffer(record))
		decoder.Decode(&tmp)

		ntz, err := fromDTO(tmp)
		if err != nil {
			return err
		}
		t.Data = append(t.Data, ntz)
	}

	return nil
}

func (t *Text) Save() error {
	encoder := gob.NewEncoder(t.File)
	for i := 0; i < t.Data.Len(); i++ {
		if err := encoder.Encode(t.Data[i].toDTO()); err != nil {
			return err
		}
		if err := encoder.Encode('\n'); err != nil {
			return err
		}
	}
	return nil
}

func (t *Text) Copy(other ZoneInfo) {
	t.Data = other.Cities()
}

func (z *Text) Cities() ByCity {
	return z.Data
}

func (u *Text) Zones() ByGMT {
	return ByGMT(u.Data)
}

func (u *Text) Configure(interface{}) error {
	return nil
}
