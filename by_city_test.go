package eventime

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	findCityExamples = map[string]struct {
		city TimeZone
		ok   bool
	}{
		"Boston":    {TimeZone{}, false},
		"Chicago":   {chicago, true},
		"London":    {london, true},
		"Madrid":    {madrid, true},
		"New York":  {newyork, true},
		"Orlando":   {TimeZone{}, false},
		"Tokyo":     {tokyo, true},
		"Vancouver": {TimeZone{}, false},
	}

	includesCityExamples = map[string]bool{
		"Boston":    false,
		"Chicago":   true,
		"London":    true,
		"Madrid":    true,
		"New York":  true,
		"Orlando":   false,
		"Tokyo":     true,
		"Vancouver": false,
	}

	filterCityTest = map[string]bool{
		"Boston":    false,
		"Chicago":   true,
		"London":    false,
		"Madrid":    false,
		"New York":  true,
		"Orlando":   false,
		"Tokyo":     true,
		"Vancouver": false,
	}
)

func TestByCity_FindCity(t *testing.T) {
	for cityName, example := range findCityExamples {
		found, ok := cities.FindCity(cityName)
		assert.Equal(t, example.ok, ok)
		assert.Equal(t, example.city, found)
	}
}

func TestByCity_Includes(t *testing.T) {
	for cityName, included := range includesCityExamples {
		assert.Equal(t, included, cities.Includes(cityName), "incorrect result for", cityName)
	}
}

func TestByCity_Filter(t *testing.T) {
	result := cities.Filter("Chicago", "New York", "Tokyo")

	for cityName, included := range filterCityTest {
		assert.Equal(t, included, result.Includes(cityName), "incorrect result for", cityName)
	}
}
