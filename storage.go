package eventime

import (
	"cloud.google.com/go/storage"
	"context"
	"encoding/json"
	"fmt"
)

type Storage struct {
	*storage.Client
	BucketName, ObjectName string
	Data                   ByCity
}

func (s *Storage) Load(cities ...string) error {
	data, err := s.Bucket(s.BucketName).Object(s.ObjectName).NewReader(context.Background())
	defer data.Close()
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(data)

	var tmp ByCity
	if err = decoder.Decode(&tmp); err != nil {
		return err
	}

	s.Data = tmp.Filter(cities...)

	return nil
}

func (s *Storage) Save() error {
	return nil
}

func (z *Storage) Zones() ByGMT {
	return ByGMT(z.Data)
}

func (z *Storage) Cities() ByCity {
	return z.Data
}

func (s *Storage) Copy(other ZoneInfo) {
	s.Data = other.Cities()
}

func (s *Storage) Configure(client interface{}) error {
	defer func() error {
		if r := recover(); r != nil {
			return fmt.Errorf("unable to configure with given client")
		}
		return nil
	}()

	s.Client = (client).(*storage.Client)
	return nil
}
