package main

import (
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"gitlab.com/1ijk/eventime"
)

var (
	zoneinfo eventime.ZoneInfo

	fromLocation, toLocation string
	fromFiletype, toFiletype int

	time, host string

	rootCommand = &cobra.Command{
		Use:   "eventime",
		Short: "plan an event across multiple regions",
	}
)

func init() {
	rootCommand.PersistentFlags().StringVar(&fromLocation, "fromLocation", "/usr/share/zoneinfo", "time zone information dictionary location")
	rootCommand.PersistentFlags().IntVar(&fromFiletype, "fromFiletype", eventime.KindUnix, "zone info dictionary type")
	planCommand.PersistentFlags().StringVar(&time, "time", "9:00AM", "time at host location")
	planCommand.PersistentFlags().StringVar(&host, "host", "Chicago", "city of host")
	copyCommand.PersistentFlags().StringVar(&toLocation, "toLocation", "zoneinfo.json", "target save location for time zone dictionary")
	copyCommand.PersistentFlags().IntVar(&toFiletype, "toFiletype", eventime.KindJOSN, "target file format for saving time zone dictionary")
}

func main() {
	rootCommand.AddCommand(planCommand)
	rootCommand.AddCommand(copyCommand)
	rootCommand.AddCommand(serverCommand)
	rootCommand.Execute()
}

func print(timetable eventime.ByGMT) error {
	var w tabwriter.Writer
	w.Init(os.Stdout, 12, 2, 0, ' ', 0)
	defer w.Flush()
	return timetable.Format(&w)
}

func load(cities ...string) error {
	tmp := make([]string, len(cities))
	for i, city := range cities {
		tmp[i] = city
	}

	var err error
	zoneinfo, err = eventime.New(fromLocation, fromFiletype)
	if err != nil {
		return err
	}

	return zoneinfo.Load(tmp...)
}
