package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/1ijk/eventime"
)

var planCommand = &cobra.Command{
	Use:   "plan",
	Short: "plan an event across multiple regions",
	PreRunE: func(cmd *cobra.Command, guests []string) error {
		return load(append(guests, host)...)
	},
	RunE: func(cmd *cobra.Command, guests []string) error {
		var event eventime.EventInfo
		event.EventTime = time
		event.HostCity = host
		event.GuestCities = guests

		timetable, err := eventime.Plan(event, zoneinfo.Cities())
		if err != nil {
			return err
		}

		return print(timetable)
	},
}
