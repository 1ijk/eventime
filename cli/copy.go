package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/1ijk/eventime"
)

var copyCommand = &cobra.Command{
	Use:   "copy",
	Short: "Copy time zone dictionary into another format",
	PreRunE: func(cmd *cobra.Command, cities []string) error {
		return load(cities...)
	},
	RunE: func(cmd *cobra.Command, guests []string) error {
		destination, err := eventime.New(toLocation, toFiletype)
		if err != nil {
			return err
		}

		destination.Copy(zoneinfo)

		return destination.Save()
	},
}
