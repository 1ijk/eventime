package main

import (
	"github.com/gorilla/mux"
	"github.com/spf13/cobra"
	"gitlab.com/1ijk/eventime/api"
	"net/http"
	t "time"
)

var (
	serverCommand = &cobra.Command{
		Use:   "server",
		Short: "run the HTTP API",
		PreRunE: func(cmd *cobra.Command, guests []string) error {
			if err := load( /* all cities */ ); err != nil {
				return err
			}
			return api.SetZoneInfo(&zoneinfo)
		},
		RunE: func(cmd *cobra.Command, guests []string) error {
			r := mux.NewRouter()

			r.HandleFunc("/cities", api.CitiesHandler).Methods("GET")
			r.HandleFunc("/plan", api.PlanningHandler).Methods("POST")
			r.HandleFunc("/slack", api.SlackHandler).Methods("POST")

			server := http.Server{
				Handler:      r,
				Addr:         "127.0.0.1:8000",
				WriteTimeout: t.Second,
				ReadTimeout:  t.Second,
			}

			return server.ListenAndServe()
		},
	}
)
