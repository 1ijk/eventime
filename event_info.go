package eventime

type EventInfo struct {
	HostCity    string   `json:"host_city"`
	GuestCities []string `json:"guest_cities"`
	EventTime   string   `json:"time"`
	EventDate   string   `json:"date,omitempty"`
}
