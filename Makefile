PROJECT := gitlab.com/1ijk/eventime

test:
	@go fmt ./...
	@go vet ./...
	@go test -cover -race ./...

build: test
	@rm -rf bin
	@go build -o bin/eventime $(PROJECT)/cli

api: build
	@echo "starting server"
	@bin/eventime server