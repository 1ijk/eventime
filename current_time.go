package eventime

import (
	"time"
)

const (
	meridian = 12 * time.Hour
	day      = 2 * meridian
)

func noon(t time.Time) time.Time {
	if isAfternoon(t) {
		t = t.Add(day)
	}

	t = t.Add(durationFromMeridian(t))

	return t
}

func isAfternoon(t time.Time) bool {
	return clockToDuration(t.Clock())/meridian > 0
}

func isMorning(t time.Time) bool {
	return !isAfternoon(t)
}

func durationFromMeridian(t time.Time) time.Duration {
	return meridian - clockToDuration(t.Clock())
}

func clockToDuration(hour, minute, second int) time.Duration {
	return time.Duration(hour)*time.Hour +
		time.Duration(minute)*time.Minute +
		time.Duration(second)*time.Second
}

func now() time.Time {
	return time.Now().UTC()
}

func set(kitchen string) (time.Time, error) {
	var t time.Time

	t, err := time.Parse(time.Kitchen, kitchen)
	if err != nil {
		return t, err
	}

	t = noon(t).Add(clockToDuration(t.Clock()))

	return t, nil
}
