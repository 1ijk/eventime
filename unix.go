package eventime

import (
	"os"
	"path/filepath"
	"strings"
)

type Unix struct {
	Directory string
	SaveTo    string
	Data      ByCity
}

func (u *Unix) Load(keys ...string) error {
	root, err := pathRedirect(u.Directory)
	if err != nil {
		return err
	}

	err = filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		timezone := strings.Split(path, `/`)

		city := timezone[len(timezone)-1]
		city = strings.ReplaceAll(city, "_", " ")
		zone := strings.ReplaceAll(path, root+`/`, ``)

		if ignore(zone) {
			return nil
		}

		if include(keys, city) {

			tz, err := NewTimeZone(city, zone)
			if err != nil {
				return err
			}

			u.Data = append(u.Data, tz)
		}

		return err
	})

	u.Data.Sort()

	return err
}

func (u *Unix) Save() error {
	file, err := os.OpenFile(u.SaveTo, os.O_RDWR|os.O_CREATE, 0744)
	if err != nil {
		return err
	}

	text := Text{File: file, Data: u.Data}
	return text.Save()
}

func (u *Unix) Zones() ByGMT {
	return ByGMT(u.Data)
}

func (u *Unix) Cities() ByCity {
	return u.Data
}

func (u *Unix) Copy(other ZoneInfo) {
	u.Data = other.Cities()
}

func (u *Unix) Configure(interface{}) error {
	return nil
}

func pathRedirect(path string) (string, error) {
	var info os.FileInfo
	var err error

	info, err = os.Lstat(path)
	if err != nil {
		return path, err
	}

	if !(info.Mode().IsRegular() || info.IsDir()) {
		path, err = os.Readlink(path)
		if err != nil {
			return path, err
		}

		return pathRedirect(path)
	}

	return path, err
}

var ignored = []string{
	"+VERSION",
	"iso3166.tab",
	"zone.tab",
}

func ignore(name string) bool {
	return include(ignored, name)
}
