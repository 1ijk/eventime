package eventime

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type TimeZone struct {
	city string
	zone *time.Location
	time time.Time
}

func NewTimeZone(cityName string, locationName string) (TimeZone, error) {
	var tz TimeZone

	location, err := time.LoadLocation(locationName)
	if err != nil {
		return tz, err
	}

	tz.city = strings.ReplaceAll(cityName, "_", " ")
	tz.zone = location
	tz.time = noon(time.Now().UTC()).In(location)
	return tz, nil
}

func (tz TimeZone) Time() time.Time {
	return tz.time
}

func (tz TimeZone) GMT() string {
	return "GMT" + strings.ReplaceAll(tz.Time().Format("-0700"), "0", "")
}

func (tz TimeZone) Offset() time.Duration {
	_, offset := tz.time.Zone()
	return time.Duration(offset) * time.Second
}

func (tz TimeZone) String() string {
	parts := []string{tz.City(), tz.Clock(), tz.GMT()}

	return strings.Join(parts, "\t")
}

func (tz TimeZone) Clock() string {
	return tz.Time().Format(time.Kitchen)
}

func (tz TimeZone) City() string {
	return tz.city
}

func (tz TimeZone) ParseClock(clock string) (TimeZone, error) {
	t, err := time.ParseInLocation(time.Kitchen, clock, tz.Time().Location())
	if err != nil {
		return tz, err
	}

	localHour := clockToDuration(tz.Time().Clock())
	utcHour := clockToDuration(t.Clock())
	adjustment := utcHour - localHour
	tz.time = tz.Time().Add(adjustment)

	return tz, nil
}

func (tz TimeZone) Set(t time.Time) TimeZone {
	tz.time = t.In(tz.Time().Location())

	return tz
}

type data struct {
	City string `json:"city"`
	Zone struct {
		Location string `json:"location"`
		GMT      string `json:"gmt"`
	} `json:"zone"`
	Time string `json:"time"`
}

func (tz *TimeZone) toDTO() data {
	var dto data
	dto.City = tz.City()
	dto.Zone.Location = tz.Time().Location().String()
	dto.Zone.GMT = tz.GMT()
	dto.Time = tz.Time().Format(time.Kitchen)
	return dto
}

func (tz *TimeZone) fromDTO(dto data) error {
	ntz, err := fromDTO(dto)
	tz.city = ntz.city
	tz.zone = ntz.zone
	tz.time = ntz.time

	return err
}

func fromDTO(dto data) (TimeZone, error) {
	ntz, err := NewTimeZone(dto.City, dto.Zone.Location)
	if err != nil {
		return ntz, err
	}

	if ntz.Time().Location().String() != dto.Zone.Location {
		return ntz, fmt.Errorf("validation failure: location")
	}

	if ntz.GMT() != dto.Zone.GMT {
		return ntz, fmt.Errorf("validation failure: GMT")
	}

	if ntz.Clock() != dto.Time {
		return ntz, fmt.Errorf("validation failure: time")
	}

	return ntz, nil
}

func (tz *TimeZone) MarshalJSON() ([]byte, error) {
	return json.Marshal(tz.toDTO())
}

func (tz *TimeZone) UnmarshalJSON(b []byte) error {
	var dto data
	if err := json.Unmarshal(b, &dto); err != nil {
		return err
	}

	return tz.fromDTO(dto)
}
