package eventime

import (
	"fmt"
	"io"
	"sort"
)

type ByGMT []TimeZone

func (zone ByGMT) Len() int {
	return len(zone)
}

func (zone ByGMT) Swap(a, b int) {
	zone[a], zone[b] = zone[b], zone[a]
}

func (zone ByGMT) Less(a, b int) bool {
	return zone[a].GMT() > zone[b].GMT()
}

func (zone ByGMT) Sort() {
	sort.Sort(zone)
}

func (zone ByGMT) String() string {
	var s string
	for _, zone := range zone {
		s += fmt.Sprintln(zone)
	}
	return s
}

func (zone ByGMT) Format(w io.Writer) error {
	if _, err := fmt.Fprintf(w, "\n%s\t%s\t%s\n", "City", "Time", "Zone"); err != nil {
		return err
	}

	if _, err := fmt.Fprintf(w, "%s\t%s\t%s\n", "----", "----", "----"); err != nil {
		return err
	}

	for _, time := range zone {
		if _, err := fmt.Fprintf(w, "%s\n", time); err != nil {
			return err
		}
	}

	if _, err := fmt.Fprintf(w, "\n"); err != nil {
		return err
	}

	return nil
}
