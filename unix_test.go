package eventime

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUnix_include(t *testing.T) {
	haystack := []string{"cup", "fork", "knife", "needle", "spinning wheel"}
	missing := []string{"apple", "spoon", "mango"}

	for _, item := range haystack {
		assert.True(t, include(haystack, item), item, "should be present")
	}

	for _, item := range missing {
		assert.False(t, include(haystack, item), item, "should be missing")
	}
}
