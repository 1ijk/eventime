package api

import (
	"encoding/json"
	"net/http"
)

// CitiesHandler returns a listing of cities and their time zone
func CitiesHandler(writer http.ResponseWriter, request *http.Request) {
	encoder := json.NewEncoder(writer)

	if err := encoder.Encode(zoneinfo.Zones()); err != nil {
		apiServerError(encoder, err)
		return
	}
}
