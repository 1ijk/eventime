package api

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"testing"
)

var ()

func TestNewSlackRequest(t *testing.T) {
	request, err := NewSlackRequest(bytes.NewBuffer([]byte(`{"text":""}`)))
	assert.NoError(t, err)
	assert.NotNil(t, request.parser)
	assert.Empty(t, request.EventInfo)
}

func TestSlackRequest_Parse(t *testing.T) {
	request, err := NewSlackRequest(bytes.NewBuffer([]byte(`{"text":"--host Berlin --time 2:00PM Madrid \"New York\" Tokyo"}`)))
	assert.NoError(t, err)
	assert.NoError(t, request.Parse())
	assert.Equal(t, "Berlin", request.HostCity)
	assert.Equal(t, "2:00PM", request.EventTime)
	assert.Equal(t, []string{"Madrid", "New York", "Tokyo"}, request.GuestCities)
}
