package api

import "gitlab.com/1ijk/eventime"

type SlackResponse struct {
	Blocks []block `json:"blocks"`
}

func NewSlackResponse(cities eventime.ByGMT) SlackResponse {
	var fields []field

	for _, city := range cities {
		fields = append(fields,
			field{"plain_text", city.City(), true},
			field{"plain_text", city.Clock() + " " + city.GMT(), true},
		)
	}

	return SlackResponse{[]block{block{"section", fields}}}
}

type block struct {
	Type   string  `json:"type"`
	Fields []field `json:"fields"`
}

type field struct {
	Type  string `json:"type"`
	Text  string `json:"text"`
	Emoji bool   `json:"emoji"`
}
