package api

import (
	"encoding/json"
	"gitlab.com/1ijk/eventime"
	"net/http"
)

// PlanningHandler reads an eventime.EventInfo from the request and returns a eventime.ByGMT in the response
func PlanningHandler(writer http.ResponseWriter, request *http.Request) {
	var info eventime.EventInfo
	var data eventime.ByGMT
	var err error

	encoder := json.NewEncoder(writer)
	decoder := json.NewDecoder(request.Body)

	if err = decoder.Decode(&info); err != nil {
		apiError(encoder, err, http.StatusBadRequest)
		return
	}

	if data, err = eventime.Plan(info, zoneinfo.Cities()); err != nil {
		apiError(encoder, err, http.StatusInternalServerError)
		return
	}

	if err := encoder.Encode(data); err != nil {
		apiServerError(encoder, err)
		return
	}
}
