package api

import (
	"encoding/json"
	"net/http"
)

func apiClientError(encoder *json.Encoder, err error) {
	apiError(encoder, err, http.StatusBadRequest)
}

func apiServerError(encoder *json.Encoder, err error) {
	apiError(encoder, err, http.StatusInternalServerError)
}

func apiError(encoder *json.Encoder, err error, code int) {
	encoder.Encode(map[string]string{"error": http.StatusText(code)})
}
