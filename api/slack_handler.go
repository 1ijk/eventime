package api

import (
	"encoding/json"
	"gitlab.com/1ijk/eventime"
	"net/http"
)

// SlackHandler parses a eventime.EventInfo from the request and returns an eventime.ByGMT in the response
func SlackHandler(writer http.ResponseWriter, request *http.Request) {
	var message *SlackRequest
	var data eventime.ByGMT
	var err error

	encoder := json.NewEncoder(writer)

	message, err = NewSlackRequest(request.Body)
	if err != nil {
		apiClientError(encoder, err)
		return
	}

	if err := message.Parse(); err != nil {
		apiClientError(encoder, err)
		return
	}

	if data, err = eventime.Plan(message.EventInfo, zoneinfo.Cities()); err != nil {
		apiServerError(encoder, err)
		return
	}

	if err := encoder.Encode(NewSlackResponse(data)); err != nil {
		apiServerError(encoder, err)
		return
	}
}
