package api

import (
	"encoding/json"
	"github.com/spf13/pflag"
	"gitlab.com/1ijk/eventime"
	"io"
)

type SlackRequest struct {
	Text   string `json:"text"`
	parser *pflag.FlagSet
	eventime.EventInfo
}

func NewSlackRequest(data io.Reader) (*SlackRequest, error) {
	var request SlackRequest
	decoder := json.NewDecoder(data)
	if err := decoder.Decode(&request); err != nil {
		return &request, err
	}
	request.parser = pflag.NewFlagSet("SlackRequest", pflag.ContinueOnError)
	return &request, nil
}

func (slack *SlackRequest) Parse() error {
	slack.parser.StringVar(&slack.HostCity, "host", "Chicago", "host city")
	slack.parser.StringVar(&slack.EventTime, "time", "9:00AM", "event time")
	if err := slack.parser.Parse(split(slack.Text)); err != nil {
		return err
	}
	slack.GuestCities = slack.parser.Args()
	return nil
}

func split(input string) []string {
	var output []string
	var quoted bool
	var word string

	for _, char := range input {
		if char == '"' {
			quoted = !quoted
			continue
		}

		if !quoted && char == ' ' {
			output = append(output, word)
			word = ""
			continue
		}

		word += string(char)
	}

	if word != "" {
		output = append(output, word)
	}

	return output
}
