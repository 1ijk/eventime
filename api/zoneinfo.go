package api

import (
	"fmt"
	"gitlab.com/1ijk/eventime"
)

var zoneinfo eventime.ZoneInfo

func SetZoneInfo(zi *eventime.ZoneInfo) error {
	zoneinfo = *zi
	defer func() error {
		if r := recover(); r != nil {
			return fmt.Errorf("fatal error: %v", r)
		}
		return nil
	}()
	return nil
}
