package eventime

import (
	"fmt"
)

func Plan(event EventInfo, zones ByCity) (ByGMT, error) {
	zones.Sort()
	var timetable ByGMT
	var guests ByGMT
	var missing []string

	host, ok := zones.FindCity(event.HostCity)
	if !ok {
		return timetable, fmt.Errorf("%s not found", event.HostCity)
	}

	host, err := host.ParseClock(event.EventTime)
	if err != nil {
		return timetable, err
	}

	for _, city := range event.GuestCities {
		guest, ok := zones.FindCity(city)
		if !ok {
			missing = append(missing, city)
			continue
		}

		guest = guest.Set(host.Time())
		if err != nil {
			missing = append(missing, city)
			continue
		}

		guests = append(guests, guest)

	}

	timetable = append(guests, host)
	timetable.Sort()

	if len(missing) > 0 {
		err = fmt.Errorf("some guest cities not found: %v", missing)
	}

	return timetable, err
}
