package eventime

import "sort"

type ByCity []TimeZone

func (zone ByCity) Len() int {
	return len(zone)
}

func (zone ByCity) Swap(a, b int) {
	zone[a], zone[b] = zone[b], zone[a]
}

func (zone ByCity) Less(a, b int) bool {
	return zone[a].City() <= zone[b].City()
}

func (zone ByCity) Sort() {
	sort.Sort(zone)
}

func (zone ByCity) searchCity(city string) int {
	return sort.Search(zone.Len(), func(i int) bool {
		return zone[i].City() >= city
	})
}

func (zone ByCity) Includes(city string) bool {
	_, ok := zone.FindCity(city)
	return ok
}

func (zone ByCity) FindCity(city string) (TimeZone, bool) {
	i := zone.searchCity(city)

	if i == zone.Len() {
		return TimeZone{}, false
	}

	found := zone[i]

	if found.city != city {
		return TimeZone{}, false
	}

	return found, true
}

func (zone ByCity) Filter(cities ...string) ByCity {
	if len(cities) == 0 {
		return zone
	}

	filtered := make(ByCity, len(cities))

	for _, city := range cities {
		if found, ok := zone.FindCity(city); ok {
			filtered = append(filtered, found)
		}
	}

	filtered.Sort()
	return filtered
}
